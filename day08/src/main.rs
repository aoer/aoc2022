use std::io::{self, Read};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;

    Ok(())
}

fn part1(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut forest: Vec<Vec<isize>> = Vec::new();
    let mut visible: Vec<Vec<bool>> = Vec::new();

    for (line_idx, line) in input.lines().enumerate() {
        forest.push(Vec::new());
        visible.push(Vec::new());
        for idx in 0..line.len() {
            forest[line_idx].push(line[idx..(idx + 1)].parse()?);
            visible[line_idx].push(false);
        }
    }

    let size = forest.len();
    for i in 0..size {
        let mut right_max_height = -1;
        let mut left_max_height = -1;
        let mut top_max_height = -1;
        let mut bottom_max_height = -1;
        for j in 0..size {
            if forest[i][j] > left_max_height {
                visible[i][j] = true;
                left_max_height = forest[i][j];
            }
            if forest[i][size - j - 1] > right_max_height {
                visible[i][size - j - 1] = true;
                right_max_height = forest[i][size - j - 1];
            }
            if forest[j][i] > top_max_height {
                visible[j][i] = true;
                top_max_height = forest[j][i];
            }
            if forest[size - j - 1][i] > bottom_max_height {
                visible[size - j - 1][i] = true;
                bottom_max_height = forest[size - j - 1][i];
            }
        }
    }

    let mut visible_trees = 0;
    for i in 0..size {
        for j in 0..size {
            if visible[i][j] == true {
                visible_trees += 1;
            }
        }
    }

    println!("The trees visible from outside are: {}", visible_trees);

    Ok(())
}

fn part2(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut forest: Vec<Vec<isize>> = Vec::new();

    for (line_idx, line) in input.lines().enumerate() {
        forest.push(Vec::new());
        for idx in 0..line.len() {
            forest[line_idx].push(line[idx..(idx + 1)].parse()?);
        }
    }

    let size = forest.len();
    let mut best_scenic_score = 0;
    for i in 0..size {
        for j in 0..size {
            let mut scenic_score = 1;
            let tree_height = forest[i][j];
            // left
            let mut distance = 0;
            for current_j in (0..j).rev() {
                distance += 1;
                if forest[i][current_j] >= tree_height {
                    break;
                }
            }
            scenic_score *= distance;

            // right
            distance = 0;
            for current_j in (j + 1)..size {
                distance += 1;
                if forest[i][current_j] >= tree_height {
                    break;
                }
            }
            scenic_score *= distance;

            // top
            distance = 0;
            for current_i in (0..i).rev() {
                distance += 1;
                if forest[current_i][j] >= tree_height {
                    break;
                }
            }
            scenic_score *= distance;

            // bottom
            distance = 0;
            for current_i in (i + 1)..size {
                distance += 1;
                if forest[current_i][j] >= tree_height {
                    break;
                }
            }
            scenic_score *= distance;

            if scenic_score > best_scenic_score {
                best_scenic_score = scenic_score;
            }
        }
    }

    println!("The highest scenic score is: {}", best_scenic_score);

    Ok(())
}
