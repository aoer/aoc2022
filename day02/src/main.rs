use std::io::{self, Read};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;

    Ok(())
}

fn part1(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut total_score = 0;
    for line in input.lines() {
        let opponent_action = &line[0..1];
        let your_action = &line[2..3];
        let score = match your_action {
            "X" => {
                1 + match opponent_action {
                    "A" => 3,
                    "B" => 0,
                    "C" => 6,
                    _ => unreachable!(),
                }
            }
            "Y" => {
                2 + match opponent_action {
                    "A" => 6,
                    "B" => 3,
                    "C" => 0,
                    _ => unreachable!(),
                }
            }
            "Z" => {
                3 + match opponent_action {
                    "A" => 0,
                    "B" => 6,
                    "C" => 3,
                    _ => unreachable!(),
                }
            }
            _ => unreachable!(),
        };
        total_score += score;
    }

    println!("Your part1 score will be: {}", total_score);

    Ok(())
}

fn part2(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut total_score = 0;
    for line in input.lines() {
        let opponent_action = &line[0..1];
        let intended_outcome = &line[2..3];
        let score = match intended_outcome {
            "X" => {
                0 + match opponent_action {
                    "A" => 3,
                    "B" => 1,
                    "C" => 2,
                    _ => unreachable!(),
                }
            }
            "Y" => {
                3 + match opponent_action {
                    "A" => 1,
                    "B" => 2,
                    "C" => 3,
                    _ => unreachable!(),
                }
            }
            "Z" => {
                6 + match opponent_action {
                    "A" => 2,
                    "B" => 3,
                    "C" => 1,
                    _ => unreachable!(),
                }
            }
            _ => unreachable!(),
        };
        total_score += score;
    }

    println!("Your part2 score will be: {}", total_score);

    Ok(())
}
