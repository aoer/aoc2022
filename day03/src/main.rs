use std::collections::HashSet;
use std::io::{self, Read};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;

    Ok(())
}

fn part1(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut total_priorities: u64 = 0;

    for line in input.lines() {
        let mut elements_in_first_compartment = HashSet::new();
        let size = line.len();
        for char in line[0..(size / 2)].chars() {
            elements_in_first_compartment.insert(char);
        }
        let mut chars_in_both_compartments = HashSet::new();
        for char in line[(size / 2)..size].chars() {
            if elements_in_first_compartment.contains(&char) {
                chars_in_both_compartments.insert(char);
            }
        }
        for char in chars_in_both_compartments {
            if char >= 'a' {
                total_priorities += char as u64 - 'a' as u64 + 1;
            } else {
                total_priorities += char as u64 - 'A' as u64 + 27;
            }
        }
    }

    println!("The total priorities are: {}", total_priorities);

    Ok(())
}

fn part2(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut total_priorities: u64 = 0;
    let mut number_in_group = 0;
    let mut elements_in_first = HashSet::new();
    let mut common_in_first_two = HashSet::new();

    for line in input.lines() {
        match number_in_group {
            0 => {
                for char in line.chars() {
                    elements_in_first.insert(char);
                }
            }
            1 => {
                for char in line.chars() {
                    if elements_in_first.contains(&char) {
                        common_in_first_two.insert(char);
                    }
                }
            }
            2 => {
                for char in line.chars() {
                    if common_in_first_two.contains(&char) {
                        if char >= 'a' {
                            total_priorities += char as u64 - 'a' as u64 + 1;
                        } else {
                            total_priorities += char as u64 - 'A' as u64 + 27;
                        }
                        break;
                    }
                }
            }
            _ => unreachable!(),
        };
        if number_in_group < 2 {
            number_in_group += 1;
        } else {
            number_in_group = 0;
            elements_in_first.clear();
            common_in_first_two.clear();
        }
    }

    println!("The total priorities of badges are: {}", total_priorities);

    Ok(())
}
