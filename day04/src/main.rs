use std::io::{self, Read};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;

    Ok(())
}

fn part1(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut number_contained: u64 = 0;

    for line in input.lines() {
        let mut assignments = line.split(&['-', ',']);
        let first_start: u64 = assignments.next().unwrap().parse()?;
        let first_end: u64 = assignments.next().unwrap().parse()?;
        let second_start: u64 = assignments.next().unwrap().parse()?;
        let second_end: u64 = assignments.next().unwrap().parse()?;
        if (first_start <= second_start && first_end >= second_end)
            || (first_start >= second_start && first_end <= second_end)
        {
            number_contained += 1
        }
    }

    println!("Number of fully contained pairs: {}", number_contained);

    Ok(())
}

fn part2(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut number_overlap: u64 = 0;

    for line in input.lines() {
        let mut assignments = line.split(&['-', ',']);
        let first_start: u64 = assignments.next().unwrap().parse()?;
        let first_end: u64 = assignments.next().unwrap().parse()?;
        let second_start: u64 = assignments.next().unwrap().parse()?;
        let second_end: u64 = assignments.next().unwrap().parse()?;
        if !(first_start > second_end || second_start > first_end) {
            number_overlap += 1
        }
    }

    println!("Number of overlapping pairs: {}", number_overlap);

    Ok(())
}
