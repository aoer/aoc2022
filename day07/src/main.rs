use std::collections::HashMap;
use std::io::{self, Read};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;

    Ok(())
}

fn part1(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut active_dirs: Vec<String> = Vec::new();
    let mut size_of_dirs: HashMap<String, u64> = HashMap::new();

    for line in input.lines() {
        let mut words = line.split(' ');
        match words.next().unwrap() {
            "$" => match words.next().unwrap() {
                "cd" => match words.next().unwrap() {
                    "/" => {
                        active_dirs.clear();
                        active_dirs.push("/".into());
                        size_of_dirs.insert("/".into(), 0);
                    }
                    ".." => {
                        active_dirs.pop().unwrap();
                    }
                    dir => {
                        active_dirs
                            .push(active_dirs.last().unwrap().clone().to_owned() + dir + "/");
                        if !size_of_dirs.contains_key(active_dirs.last().unwrap()) {
                            size_of_dirs.insert(active_dirs.last().unwrap().clone(), 0);
                        }
                    }
                },
                "ls" => continue,
                _ => unreachable!(),
            },
            "dir" => continue,
            number => {
                let size: u64 = number.parse()?;
                for dir in &active_dirs {
                    size_of_dirs.insert(dir.clone(), size_of_dirs[dir] + size);
                }
            }
        }
    }

    let mut size_small_dirs = 0;
    for &size in size_of_dirs.values() {
        if size <= 100000 {
            size_small_dirs += size;
        }
    }

    println!(
        "The total size of directories with size at most 100000 is: {}",
        size_small_dirs
    );

    Ok(())
}

fn part2(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut active_dirs: Vec<String> = Vec::new();
    let mut size_of_dirs: HashMap<String, u64> = HashMap::new();

    for line in input.lines() {
        let mut words = line.split(' ');
        match words.next().unwrap() {
            "$" => match words.next().unwrap() {
                "cd" => match words.next().unwrap() {
                    "/" => {
                        active_dirs.clear();
                        active_dirs.push("/".into());
                        size_of_dirs.insert("/".into(), 0);
                    }
                    ".." => {
                        active_dirs.pop().unwrap();
                    }
                    dir => {
                        active_dirs
                            .push(active_dirs.last().unwrap().clone().to_owned() + dir + "/");
                        if !size_of_dirs.contains_key(active_dirs.last().unwrap()) {
                            size_of_dirs.insert(active_dirs.last().unwrap().clone(), 0);
                        } else {
                            dbg!(active_dirs.last().unwrap());
                        }
                    }
                },
                "ls" => continue,
                _ => unreachable!(),
            },
            "dir" => continue,
            number => {
                let size: u64 = number.parse()?;
                for dir in &active_dirs {
                    size_of_dirs.insert(dir.clone(), size_of_dirs[dir] + size);
                }
            }
        }
    }

    let required_size = *size_of_dirs.get("/".into()).unwrap() - 40_000_000;
    let mut smallest_size_big_enough = std::u64::MAX;
    for &size in size_of_dirs.values() {
        if size >= required_size {
            if size < smallest_size_big_enough {
                smallest_size_big_enough = size;
            }
        }
    }

    println!(
        "The size of the smallest directory that frees up enough space is: {}",
        smallest_size_big_enough
    );

    Ok(())
}
