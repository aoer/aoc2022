use std::io::{self, Read};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;

    Ok(())
}

fn part1(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut number_before_start = 0;

    for line in input.lines() {
        'position: for idx in 0..(line.len() - 4) {
            let candidate_chars: Vec<char> = line[idx..(idx + 4)].chars().collect();
            for first_idx in 0..(candidate_chars.len() - 1) {
                for second_idx in (first_idx + 1)..candidate_chars.len() {
                    if candidate_chars[first_idx] == candidate_chars[second_idx] {
                        continue 'position;
                    }
                }
            }
            number_before_start = idx + 4;
            break 'position;
        }
    }

    println!(
        "Number of chars before start-of-package marker is: {}",
        number_before_start
    );

    Ok(())
}

fn part2(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut number_before_start = 0;

    for line in input.lines() {
        'position: for idx in 0..(line.len() - 14) {
            let candidate_chars: Vec<char> = line[idx..(idx + 14)].chars().collect();
            for first_idx in 0..(candidate_chars.len() - 1) {
                for second_idx in (first_idx + 1)..candidate_chars.len() {
                    if candidate_chars[first_idx] == candidate_chars[second_idx] {
                        continue 'position;
                    }
                }
            }
            number_before_start = idx + 14;
            break 'position;
        }
    }

    println!(
        "Number of chars before start-of-message marker is: {}",
        number_before_start
    );

    Ok(())
}
