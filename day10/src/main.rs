use std::io::{self, Read};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;

    Ok(())
}

fn part1(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut total_signal_strength = 0;
    let mut current_time = 1;
    let mut register_value = 1;
    let mut next_interesting_time = 20;

    for line in input.lines() {
        let instruction: Vec<&str> = line.split(' ').collect();
        let update_value = match instruction[0] {
            "noop" => {
                current_time += 1;
                0
            }
            "addx" => {
                current_time += 2;
                instruction[1].parse()?
            }
            _ => unreachable!(),
        };
        if current_time > next_interesting_time {
            total_signal_strength += register_value * next_interesting_time;
            next_interesting_time += 40;
        }
        register_value += update_value;
    }

    println!(
        "The sum of the signal strengths is: {}",
        total_signal_strength
    );

    Ok(())
}

fn matrix_to_string(matrix: Vec<Vec<bool>>) -> String {
    let mut output = String::from("");
    for i in 0..matrix.len() {
        for j in 0..matrix[i].len() {
            output += match matrix[i][j] {
                true => "#",
                false => ".",
            };
        }
        output += "\n";
    }
    return output;
}

fn part2(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut horizontal_position = 0;
    let mut register_value = 1;
    let mut output: Vec<Vec<bool>> = Vec::new();
    output.push(Vec::new());

    for line in input.lines() {
        let difference = register_value - horizontal_position;
        output
            .last_mut()
            .unwrap()
            .push(difference <= 1 && difference >= -1);
        horizontal_position += 1;
        if horizontal_position >= 40 {
            horizontal_position = 0;
            output.push(Vec::new());
        }

        let instruction: Vec<&str> = line.split(' ').collect();
        let update_value = match instruction[0] {
            "noop" => 0,
            "addx" => {
                let difference = register_value - horizontal_position;
                output
                    .last_mut()
                    .unwrap()
                    .push(difference <= 1 && difference >= -1);
                horizontal_position += 1;
                if horizontal_position >= 40 {
                    horizontal_position = 0;
                    output.push(Vec::new());
                }

                instruction[1].parse()?
            }
            _ => unreachable!(),
        };
        register_value += update_value;
    }

    println!("The output is: \n{}", matrix_to_string(output));

    Ok(())
}
