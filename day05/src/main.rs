use std::{
    collections::VecDeque,
    io::{self, Read},
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;

    Ok(())
}

fn part1(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut setup = true;
    let mut stacks: Vec<VecDeque<char>> = Vec::new();
    let mut number_of_stacks = 0;

    for (line_idx, line) in input.lines().enumerate() {
        if line_idx == 0 {
            number_of_stacks = (line.len() + 1) / 4;
            for _ in 0..number_of_stacks {
                stacks.push(VecDeque::new());
            }
        }
        if setup {
            if line == "" {
                setup = false;
            } else {
                for stack_idx in 0..number_of_stacks {
                    let char = line[(stack_idx * 4 + 1)..(stack_idx * 4 + 2)]
                        .chars()
                        .next()
                        .unwrap();
                    if char == '1' {
                        break;
                    }
                    if char == ' ' {
                        continue;
                    }
                    stacks[stack_idx].push_front(char);
                }
            }
        } else {
            let action: Vec<&str> = line.split(' ').collect();
            let amount: u64 = action[1].parse()?;
            let mut from: usize = action[3].parse()?;
            from -= 1;
            let mut to: usize = action[5].parse()?;
            to -= 1;
            for _ in 0..amount {
                let moving_crate = stacks[from].pop_back().unwrap();
                stacks[to].push_back(moving_crate);
            }
        }
    }

    let mut top_crates = String::new();
    for stack_idx in 0..number_of_stacks {
        top_crates += &String::from(stacks[stack_idx].pop_back().unwrap());
    }

    println!("The top crates for part1 are: {}", top_crates);

    Ok(())
}

fn part2(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut setup = true;
    let mut stacks: Vec<VecDeque<char>> = Vec::new();
    let mut number_of_stacks = 0;

    for (line_idx, line) in input.lines().enumerate() {
        if line_idx == 0 {
            number_of_stacks = (line.len() + 1) / 4;
            for _ in 0..number_of_stacks {
                stacks.push(VecDeque::new());
            }
        }
        if setup {
            if line == "" {
                setup = false;
            } else {
                for stack_idx in 0..number_of_stacks {
                    let char = line[(stack_idx * 4 + 1)..(stack_idx * 4 + 2)]
                        .chars()
                        .next()
                        .unwrap();
                    if char == '1' {
                        break;
                    }
                    if char == ' ' {
                        continue;
                    }
                    stacks[stack_idx].push_front(char);
                }
            }
        } else {
            let action: Vec<&str> = line.split(' ').collect();
            let amount: u64 = action[1].parse()?;
            let mut from: usize = action[3].parse()?;
            from -= 1;
            let mut to: usize = action[5].parse()?;
            to -= 1;
            let mut moving_crates: Vec<char> = Vec::new();
            for _ in 0..amount {
                moving_crates.push(stacks[from].pop_back().unwrap());
            }
            for _ in 0..amount {
                stacks[to].push_back(moving_crates.pop().unwrap());
            }
        }
    }

    let mut top_crates = String::new();
    for stack_idx in 0..number_of_stacks {
        top_crates += &String::from(stacks[stack_idx].pop_back().unwrap());
    }

    println!("The top crates for part2 are: {}", top_crates);

    Ok(())
}
