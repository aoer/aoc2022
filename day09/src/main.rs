use std::io::{self, Read};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;

    Ok(())
}

fn part1(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let size = 1000;
    let mut head_pos = (500, 500);
    let mut tail_pos = (500, 500);
    let mut visited: Vec<Vec<bool>> = Vec::new();
    for idx in 0..size {
        visited.push(Vec::new());
        for _ in 0..size {
            visited[idx].push(false);
        }
    }
    visited[tail_pos.0][tail_pos.1] = true;

    for line in input.lines() {
        let instruction: Vec<&str> = line.split(' ').collect();
        let number: u64 = instruction[1].parse()?;
        for _ in 0..number {
            match instruction[0] {
                "L" => {
                    head_pos.1 -= 1;
                    if tail_pos.1 - head_pos.1 > 1 {
                        tail_pos.1 -= 1;
                        tail_pos.0 = head_pos.0;
                    }
                }
                "R" => {
                    head_pos.1 += 1;
                    if head_pos.1 - tail_pos.1 > 1 {
                        tail_pos.1 += 1;
                        tail_pos.0 = head_pos.0;
                    }
                }
                "U" => {
                    head_pos.0 += 1;
                    if head_pos.0 - tail_pos.0 > 1 {
                        tail_pos.0 += 1;
                        tail_pos.1 = head_pos.1;
                    }
                }
                "D" => {
                    head_pos.0 -= 1;
                    if tail_pos.0 - head_pos.0 > 1 {
                        tail_pos.0 -= 1;
                        tail_pos.1 = head_pos.1;
                    }
                }
                _ => unreachable!(),
            }

            visited[tail_pos.0][tail_pos.1] = true;
        }
    }

    let mut number_visted_cells = 0;
    for i in 0..size {
        for j in 0..size {
            if visited[i][j] {
                number_visted_cells += 1;
            }
        }
    }

    println!(
        "The number of visited cells for part 1 is: {}",
        number_visted_cells
    );

    Ok(())
}

fn part2(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let size = 1000;
    let mut rope_positions: Vec<(usize, usize)> = vec![(500, 500); 10];
    let mut visited: Vec<Vec<bool>> = Vec::new();
    for idx in 0..size {
        visited.push(Vec::new());
        for _ in 0..size {
            visited[idx].push(false);
        }
    }
    visited[rope_positions[9].0][rope_positions[9].1] = true;

    for line in input.lines() {
        let instruction: Vec<&str> = line.split(' ').collect();
        let number: u64 = instruction[1].parse()?;
        for _ in 0..number {
            match instruction[0] {
                "L" => rope_positions[0].1 -= 1,
                "R" => rope_positions[0].1 += 1,
                "U" => rope_positions[0].0 += 1,
                "D" => rope_positions[0].0 -= 1,
                _ => unreachable!(),
            }

            for idx in 1..rope_positions.len() {
                let diff = (
                    (rope_positions[idx - 1].0 - rope_positions[idx].0) as isize,
                    (rope_positions[idx - 1].1 - rope_positions[idx].1) as isize,
                );
                match diff {
                    (2, 2) => {
                        rope_positions[idx].0 += 1;
                        rope_positions[idx].1 += 1;
                    }
                    (-2, 2) => {
                        rope_positions[idx].0 -= 1;
                        rope_positions[idx].1 += 1;
                    }
                    (2, -2) => {
                        rope_positions[idx].0 += 1;
                        rope_positions[idx].1 -= 1;
                    }
                    (-2, -2) => {
                        rope_positions[idx].0 -= 1;
                        rope_positions[idx].1 -= 1;
                    }
                    (2, _) => {
                        rope_positions[idx].0 += 1;
                        rope_positions[idx].1 = rope_positions[idx - 1].1;
                    }
                    (-2, _) => {
                        rope_positions[idx].0 -= 1;
                        rope_positions[idx].1 = rope_positions[idx - 1].1;
                    }
                    (_, 2) => {
                        rope_positions[idx].1 += 1;
                        rope_positions[idx].0 = rope_positions[idx - 1].0
                    }
                    (_, -2) => {
                        rope_positions[idx].1 -= 1;
                        rope_positions[idx].0 = rope_positions[idx - 1].0;
                    }
                    _ => (),
                }
            }

            visited[rope_positions.last().unwrap().0][rope_positions.last().unwrap().1] = true;
        }
    }

    let mut number_visted_cells = 0;
    for i in 0..size {
        for j in 0..size {
            if visited[i][j] {
                number_visted_cells += 1;
            }
        }
    }

    println!(
        "The number of visited cells for part 2 is: {}",
        number_visted_cells
    );

    Ok(())
}
