use std::io::{self, Read};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;

    Ok(())
}

fn part1(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut calories = 0;
    let mut max_calories = 0;
    for line in input.lines() {
        if line == "" {
            if calories > max_calories {
                max_calories = calories;
            }
            calories = 0;
        } else {
            let current_calories: u64 = line.parse()?;
            calories += current_calories;
        }
    }
    if calories > max_calories {
        max_calories = calories;
    }

    println!("Maximum calories carried by one elf: {}", max_calories);

    Ok(())
}

fn part2(input: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut food_of_elves: Vec<u64> = Vec::new();
    let mut calories = 0;
    for line in input.lines() {
        if line == "" {
            food_of_elves.push(calories);
            calories = 0;
        } else {
            let current_calories: u64 = line.parse()?;
            calories += current_calories;
        }
    }
    food_of_elves.push(calories);

    food_of_elves.sort();

    let top_three_calories =
        food_of_elves.pop().unwrap() + food_of_elves.pop().unwrap() + food_of_elves.pop().unwrap();
    println!(
        "Calories carried by top three elves: {}",
        top_three_calories
    );

    Ok(())
}
